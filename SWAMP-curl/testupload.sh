#!/bin/bash

SWAMPUSER="admin-s";
SWAMPPASS="blanc";
CSA="maccfws.dyndns.cern.ch/swamp-web-server/public";

##
#login
##

# create private files
umask 0077
# log in to CSA
curl -k -f -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -d "{\"username\":\"$SWAMPUSER\",\"password\":\"$SWAMPPASS\"}" \
  https://$CSA/login > rws-userinfo.txt
# find my user UUID
perl -n -e 'print $1 if (/\"user_uid\":\"([\w-]+)\"/);' \
  < rws-userinfo.txt > swamp-user-uuid.txt
export SWAMP_USER_UUID=`cat swamp-user-uuid.txt`
# look up additional user info (email address, etc.)
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/users/$SWAMP_USER_UUID > swamp-user-details.txt

##
#Upload
##

file="jens.tar"
SWAMP_PACKAGE_NAME="testing";


# upload my package tarball
# using hello.tar.gz from https://uofi.box.com/swamp-c-hello

curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -X POST \
  -F file=@$file \
  -F user_uid=$SWAMP_USER_UUID \
  https://$CSA/packages/versions/upload > swamp-uploaded-file.txt

# get the destination path UUID
perl -n -e 'print $1 if (/\"destination_path\":\"([\w-]+)\"/);' \
  < swamp-uploaded-file.txt > swamp-dest-path.txt
export SWAMP_DEST_PATH=`cat swamp-dest-path.txt`

# create the package
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -d "{\"package_sharing_status\":\"private\",\
       \"name\":\"$SWAMP_PACKAGE_NAME\",\
       \"description\":\"\",\
       \"external_url\":\"\",\
       \"package_type_id\":1}" \
  https://$CSA/packages > swamp-package.txt

# get the package UUID
perl -n -e 'print $1 if (/\"package_uuid\":\"([\w-]+)\"/);' \
  < swamp-package.txt > swamp-package-uuid.txt
export SWAMP_PACKAGE_UUID=`cat swamp-package-uuid.txt`

# create the package version
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -v \
  -d "{\"version_string\":\"1.0\", \
       \"version_sharing_status\":\"protected\", \
       \"package_uuid\":\"7962454c-b4fd-47c5-b843-aa7e3057b006\", \
       \"notes\":\"\", \
       \"source_path\":\"jeans.tar\", \
       \"config_dir\":\"\", \
       \"config_cmd\":\"\", \
       \"config_opt\":\"\", \
       \"build_file\":\"\", \
       \"build_system\":\"\", \
       \"build_target\":\"\", \
       \"build_dir\":\"\", \
       \"build_opt\":\"\", \
       \"package_path\":\"63d488ef-7dae-4a32-8a25-4654a94214a9/jeans.tar\"}" \
  https://maccfws.dyndns.cern.ch/swamp-web-server/public/packages/versions/store > swamp-pkgver.txt

# get package version UUID
perl -n -e 'print $1 if (/\"package_version_uuid\":\"([\w-]+)\"/);' \
  < swamp-pkgver.txt > swamp-pkgver-uuid.txt
export SWAMP_PKGVER_UUID=`cat swamp-pkgver-uuid.txt`
# share package version with $SWAMP_PROJECT_UUID
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -X PUT \
  -d "projects[0][project_uid]=$SWAMP_PROJECT_UUID" \
  https://$CSA/packages/versions/$SWAMP_PKGVER_UUID/sharing

