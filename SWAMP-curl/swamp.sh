#!/bin/bash

##### Functions


function login
{

# create private files
umask 0077
# log in to CSA
curl -k -f -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -d "{\"username\":\"$SWAMPUSER\",\"password\":\"$SWAMPPASS\"}" \
  https://$CSA/login > rws-userinfo.txt
# find my user UUID
perl -n -e 'print $1 if (/\"user_uid\":\"([\w-]+)\"/);' \
  < rws-userinfo.txt > swamp-user-uuid.txt
export SWAMP_USER_UUID=`cat swamp-user-uuid.txt`
# look up additional user info (email address, etc.)
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/users/$SWAMP_USER_UUID > swamp-user-details.txt
}   # end of login


function listpackages
{
# list public packages (including UUIDs)
curl -k -f https://$CSA/packages/public > swamp-public-packages.txt
# list packages shared with my project
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/packages/protected/$SWAMP_PROJECT_UUID > \
  swamp-protected-packages.txt
# list package types (C/C++, Java Source Code, etc.)
curl -k -f https://$CSA/packages/types > swamp-package-types.txt
}    # end of listpackages


function findmyprojects
{
# find "MyProject"
# get my project memberships
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/users/$SWAMP_USER_UUID/projects/trial \
  > swamp-myproject.txt
# get UUID for "MyProject"
perl -n -e 'print $1 if (/\"project_uid\":\"([\w-]+)\"/);' \
  < swamp-myproject.txt > swamp-project-uuid.txt
export SWAMP_PROJECT_UUID=`cat swamp-project-uuid.txt`
# get my other project memberships (if any)
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/users/$SWAMP_USER_UUID/projects > swamp-projects.txt
}


function listtools
{
# list public tools (including UUIDs)
curl -k -f https://$CSA/tools/public > swamp-public-tools.txt
# list restricted tools (which require accepting a license)
curl -k -f https://$CSA/tools/restricted > swamp-restricted-tools.txt
}


function listassessments
{
# list assessments (including UUIDs) per project
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/projects/$SWAMP_PROJECT_UUID/assessment_runs \
  > swamp-assessments.txt
}


function listassessmentresults
{
# list assessment results per project
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  https://$CSA/projects/$SWAMP_PROJECT_UUID/assessment_results \
  > swamp-assessment-results.txt
perl -n -l -e 'print "$1" while (/\"assessment_result_uuid\":\"([\w-]+)\"/g);' \
  < swamp-assessment-results.txt > swamp-assessment-result-uuids.txt
}


function uploadpackage
{
# upload my package tarball
# using hello.tar.gz from https://uofi.box.com/swamp-c-hello

curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -X POST \
  -F file=@$file \
  -F user_uid=$SWAMP_USER_UUID \
  https://$CSA/packages/versions/upload > swamp-uploaded-file.txt

# get the destination path UUID
perl -n -e 'print $1 if (/\"destination_path\":\"([\w-]+)\"/);' \
  < swamp-uploaded-file.txt > swamp-dest-path.txt
export SWAMP_DEST_PATH=`cat swamp-dest-path.txt`

# create the package
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -d "{\"package_sharing_status\":\"private\",\
       \"name\":\"$SWAMP_PACKAGE_NAME\",\
       \"description\":\"\",\
       \"external_url\":\"\",\
       \"package_type_id\":1}" \
  https://$CSA/packages > swamp-package.txt

# get the package UUID
perl -n -e 'print $1 if (/\"package_uuid\":\"([\w-]+)\"/);' \
  < swamp-package.txt > swamp-package-uuid.txt
export SWAMP_PACKAGE_UUID=`cat swamp-package-uuid.txt`

# create the package version
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/json; charset=UTF-8" \
  -X POST \
  -d "{\"version_string\":\"1.0\", \
       \"version_sharing_status\":\"protected\", \
       \"package_uuid\":\"$SWAMP_PACKAGE_UUID\", \
       \"notes\":\"\", \
       \"source_path\":\"$file\", \
       \"config_dir\":\"\", \
       \"config_cmd\":\"\", \
       \"config_opt\":\"\", \
       \"build_file\":\"\", \
       \"build_system\":\"\", \
       \"build_target\":\"\", \
       \"build_dir\":\"\", \
       \"build_opt\":\"\", \
       \"package_path\":\"$SWAMP_DEST_PATH/$file\"}" \
  https://$CSA/packages/versions/store > swamp-pkgver.txt

# get package version UUID
perl -n -e 'print $1 if (/\"package_version_uuid\":\"([\w-]+)\"/);' \
  < swamp-pkgver.txt > swamp-pkgver-uuid.txt
export SWAMP_PKGVER_UUID=`cat swamp-pkgver-uuid.txt`

# share package version with $SWAMP_PROJECT_UUID
curl -k -f -b csa-cookie-jar.txt -c csa-cookie-jar.txt \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -X PUT \
  -d "projects[0][project_uid]=$SWAMP_PROJECT_UUID" \
  https://$CSA/packages/versions/$SWAMP_PKGVER_UUID/sharing
}


function usage()
{
    echo ""
    echo ""
    echo "./swamp-curl.sh"
    echo "\t-h --help"
    echo "\t--user=$SWAMPUSER"
    echo "\t--pass=$SWAMPPASS"
    echo "\t--csa=$CSA"
    echo "\t--list-packages"
    echo "\t--find-my-projects"
    echo "\t--list-tools"
    echo "\t--list-assessments"
    echo "\t--list-assessments-results"
    echo "\t--upload-package=<pkg.tar> <pkg name>"
    echo ""
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        --user)
            SWAMPUSER=$VALUE
            ;;
        --pass)
            SWAMPPASS=$VALUE
            ;;
        --csa)
            CSA=$VALUE/swamp-web-server/public
            ;;
        --list-packages)
            login
	    listpackages
	    ;;
        --find-my-projects)
            login
            findmyprojects
            ;;
        --list-tools)
            login
            listtools
            ;;
        --list-assessments)
            login
            findmyprojects
            listassessments
            ;;
        --list-assessments-results)
            login
            findmyprojects
            listassessmentresults
            ;;
        --upload-package)
            login
            uploadpackage
            ;;
        --package-file)
            file=$VALUE
            ;;
        --package-name)
            SWAMP_PACKAGE_NAME=$VALUE
            ;;


        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

echo "SWAMPUSER is $SWAMPUSER";
echo "SWAMPPASS is $SWAMPPASS";
echo "CSA is $CSA";
echo $file
echo $SWAMP_PACKAGE_NAME
